#!/usr/bin/env ruby
# frozen_string_literal: true

require 'pathname'

# TODO: add --verbose option

# Take a shot of the marvelous screen
class Screenshot
  def self.which?(executable)
    ENV['PATH'].split(File::PATH_SEPARATOR).any? do |directory|
      File.executable?(File.join(directory, executable.to_s))
    end
  end

  attr_reader :shotters

  def initialize(shotters = Shotters.new.all)
    @shotters = shotters
  end

  def full
    [shotter.exec, shotter.common, shotter.full, shotter.full].join ' '
  end

  def partial
    [shotter.exec, shotter.common, shotter.partial, shotter.folder].join ' '
  end

  def info
    "Shotters available: #{shotters.keys.join(', ')}"
  end

  private

  # list of all shotters found
  def shotters_available
    shotters.find_all do |shotter|
      x = shotter.last
      x.exec if Screenshot.which? x.exec
    end
  end

  # select default shotter
  def shotter
    shotters_available.first.last
  end
end

# All available shotters
class Shotters
  require 'ostruct'

  FOLDER = Pathname.new(Dir.home).join('Pictures')
  PREFERRED_FORMAT = 'png'
  CURRENT_TIME = Time.new.strftime '%d-%m-%y-%I-%M'

  def all
    {
      scrot: OpenStruct.new(exec: 'scrot',
                            full: '--focused --silent', partial: '--select --silent', common: '--border --quality=100',
                            folder: FOLDER.join(screenshot_name)),
      main: OpenStruct.new(exec: 'maim',
                           full: '', partial: '--select',
                           folder: FOLDER.join(screenshot_name)),
      flameshot: OpenStruct.new(exec: 'flameshot',
                                full: '', partial: '--select',
                                folder: FOLDER.join(screenshot_name))
    }
  end

  private

  def screenshot_name
    "screenshot-#{CURRENT_TIME}.#{PREFERRED_FORMAT}"
  end
end

module Cli
  require 'optparse'

  def self.options
    shotter = Screenshot.new

    OptionParser.new do |opts|
      opts.banner = 'Usage: shot [options]'

      opts.on('-f', '--full', 'full screen shot') do
        system shotter.full
      end

      opts.on('-p', '--partial', 'partial screen shot') do
        system shotter.partial
      end

      opts.on('-i', '--info', 'shotters information') do
        puts shotter.info
      end
    end
  end
end

Cli.options.parse! ['--help'] if ARGV.empty?
Cli.options.parse!
